package com.msadal.puzzle.api;

import java.io.*;
import java.net.Socket;

import static com.msadal.puzzle.api.GameServerAPI.RemoteCommand.*;

public class GameServerAPI implements GameAPI {

    public enum RemoteCommand {
        SEND(1),
        RECEIVE(2),
        RECEIVE_INTERRUPTIBLE(3);

        private int value;

        RemoteCommand(int value) {
            this.value = value;
        }

        public int toInt() {
            return value;
        }
    }

    private Socket socket;
    private DataOutputStream outputStream;
    private BufferedReader reader;

    public GameServerAPI(Socket socket) {
        try {
            this.socket = socket;
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.outputStream = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            close();
            throw new RuntimeException("Failed to construct GameServerAPI for socket", e);

        }
    }

    @Override
    public void sendMessage(String message) {
        try {
            outputStream.writeInt(SEND.toInt());
            outputStream.writeUTF(message + "%n");
            outputStream.flush();
        } catch (IOException e) {
            System.out.println("Error sending data through socket");
        }
    }

    @Override
    public String receiveData(String message) {
        try {
            outputStream.writeInt(RECEIVE.toInt());
            outputStream.writeUTF(message);
            outputStream.flush();
            return reader.readLine();
        } catch (IOException e) {
            System.out.println("Error receiving data from socket");
            return null;
        }
    }

    @Override
    public String receiveDataInterruptible(String message) throws InterruptedException {
        try {
            outputStream.writeInt(RECEIVE_INTERRUPTIBLE.toInt());
            outputStream.writeUTF(message);
            outputStream.flush();
            while (!Thread.currentThread().isInterrupted() && !reader.ready()) {
                Thread.sleep(10);
            }
            if (!Thread.currentThread().isInterrupted()) {
                return reader.readLine();
            }
            throw new InterruptedIOException();
        } catch (InterruptedException e) {
            throw e;
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void close() {
        try {
            if (reader != null) {
                reader.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
            socket.close();
        } catch (IOException e) {
            System.out.println("Error closing socket");
        }
    }

}
