package com.msadal.puzzle.api.player;

import com.msadal.puzzle.api.GameAPI;
import com.msadal.puzzle.domain.Shape;

public class HumanGameAPI implements PlayerGameAPI {

    protected GameAPI api;

    public HumanGameAPI(GameAPI api) {
        this.api = api;
    }

    @Override
    public void sendMessage(String message) {
        api.sendMessage(message);
    }

    @Override
    public void countingDown(long millis) {
        try {
            sendMessage("Ready... Steady...");
            Thread.sleep(millis / 3);
            sendMessage("Ro!");
            Thread.sleep(millis / 3);
            sendMessage("Sham!");
            Thread.sleep(millis / 3);
            sendMessage("Bo!");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            sendMessage("Bo!");
        }
    }

    @Override
    public Shape readShape() {
        try {
            String input = api.receiveDataInterruptible("Choose Rock(1) | Paper(2) | Scissors(3): ");
            if (input != null) {
                int number = Integer.parseInt(input);
                return Shape.getShape(number);
            }
        } catch (NumberFormatException | InterruptedException e) {
            // do nothing and return None
        }
        sendMessage("");
        return Shape.None;
    }
}
