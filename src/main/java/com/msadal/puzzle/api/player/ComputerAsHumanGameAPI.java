package com.msadal.puzzle.api.player;

import com.msadal.puzzle.api.GameAPI;
import com.msadal.puzzle.domain.Shape;

public class ComputerAsHumanGameAPI extends HumanGameAPI {

    public ComputerAsHumanGameAPI(GameAPI api) {
        super(api);
    }

    @Override
    public Shape readShape() {
        api.sendMessage("Choose Rock(1) | Paper(2) | Scissors(3): ");
        Shape shape = Shape.randomShape();
        api.sendMessage(shape.getValue() + "");
        return shape;
    }
}
