package com.msadal.puzzle.api.player;

import com.msadal.puzzle.domain.Shape;

public class DumbComputerGameAPI implements PlayerGameAPI {
    @Override
    public void sendMessage(String message) {
        // send to /dev/null
    }

    @Override
    public void countingDown(long millis) {
        // do nothing
    }

    @Override
    public Shape readShape() {
        return Shape.randomShape();
    }
}
