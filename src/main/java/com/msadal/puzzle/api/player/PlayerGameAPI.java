package com.msadal.puzzle.api.player;

import com.msadal.puzzle.domain.Shape;

public interface PlayerGameAPI {

    void sendMessage(String message);
    void countingDown(long millis);
    Shape readShape();
}
