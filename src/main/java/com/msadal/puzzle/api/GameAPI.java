package com.msadal.puzzle.api;

public interface GameAPI {

    void sendMessage(String message);
    String receiveData(String message);
    String receiveDataInterruptible(String message) throws InterruptedException;
    void close();
}
