package com.msadal.puzzle.api;

import com.msadal.puzzle.api.GameAPI;

import java.io.Console;
import java.io.IOException;
import java.io.InterruptedIOException;

public class GameShellAPI implements GameAPI {

    private Console console;

    public GameShellAPI() {
        console = System.console();
        if (console == null) {
            throw new RuntimeException("Error: Console unavailable");
        }
    }

    @Override
    public void sendMessage(String message) {
        console.printf(message + "%n");
    }

    @Override
    public String receiveData(String message) {
        return console.readLine(message);
    }

    @Override
    public String receiveDataInterruptible(String message) throws InterruptedException {
        try {
            console.printf(message);
            while (!Thread.currentThread().isInterrupted() && !console.reader().ready()) {
                Thread.sleep(10);
            }
            if (!Thread.currentThread().isInterrupted()) {
                return console.readLine();
            }
            console.printf("%n");
            throw new InterruptedIOException();
        } catch (InterruptedException e) {
            console.printf("%n");
            throw e;
        } catch (IOException e) {
            console.printf("%mError reading data%n");
            return null;
        }
    }

    @Override
    public void close() {
        // don't close anything here
    }
}
