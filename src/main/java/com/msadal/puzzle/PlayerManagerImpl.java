package com.msadal.puzzle;


import com.msadal.puzzle.dao.PlayerDao;
import com.msadal.puzzle.dao.SerialPlayerDaoImpl;
import com.msadal.puzzle.domain.Player;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerManagerImpl implements  PlayerManager {

    private Map<String, Player> playersMap = new ConcurrentHashMap<>();
    private PlayerDao dao;

    public PlayerManagerImpl(Path storePath) {
        this.dao = new SerialPlayerDaoImpl(storePath);
        dao.load().stream().forEach(p -> {playersMap.put(p.getLogin(), p);});
    }

    @Override
    public Player get(String login) {
        return playersMap.get(login);
    }

    @Override
    public void put(Player player) {
        playersMap.put(player.getLogin(), player);
    }

    @Override
    public List<Player> getAllPlayers() {
        return new LinkedList<>(playersMap.values());
    }

    @Override
    public Player chooseOpponent(String yourLogin) {
        Optional<Player> opponent = playersMap.values().stream()
                .filter(p -> !p.getLogin().equals(yourLogin))
                .findAny();
        if (!opponent.isPresent()) {
            Player computer = new Player("computer", "Deep Blue");
            put(computer);
            return computer;
        }
        return opponent.get();
    }

    @Override
    public void shutdown() {
        List<Player> list2Store = new LinkedList<>(playersMap.values());
        dao.store(list2Store);
    }
}
