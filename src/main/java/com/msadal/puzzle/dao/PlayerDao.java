package com.msadal.puzzle.dao;

import com.msadal.puzzle.domain.Player;

import java.util.List;

public interface PlayerDao {

    List<Player> load();
    void store(List<Player> list2Store);
}
