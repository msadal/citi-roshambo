package com.msadal.puzzle.dao;

import com.msadal.puzzle.domain.Player;

import java.io.*;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SerialPlayerDaoImpl implements PlayerDao {

    private Path storePath;

    public SerialPlayerDaoImpl(Path storePath) {
        if (!Files.exists(storePath.getParent())) {
            try {
                Files.createDirectory(storePath.getParent());
            } catch (IOException e) {
                throw new RuntimeException("Failed to initialize SerialPlayerDaoImpl");
            }
        }
        this.storePath = storePath;
    }

    public List<Player> load() {
        try (ObjectInputStream objIStream = new ObjectInputStream(new FileInputStream(storePath.toFile()))) {
            List<Player> loadedList = (List<Player>) objIStream.readObject();
            return loadedList;
        } catch (FileNotFoundException e) {
            System.out.println("player.store db not found");
        } catch (Exception e) {
            System.out.println("Failed to load players.");
        }
        return new LinkedList<>();
    }

    public void store(List<Player> list2Store) {
        File tmpFile = null;
        try {
            Path dir = storePath.getParent();
            if (!Files.exists(dir)) {
                Files.createDirectories(storePath);
            }
            tmpFile = File.createTempFile("store", null, dir.toFile());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        FileOutputStream fileOStream = null;
        try (ObjectOutputStream objOStream = new ObjectOutputStream(fileOStream = new FileOutputStream(tmpFile))) {
            objOStream.writeObject(list2Store);
            objOStream.flush();
            int attempts = 0;

            while (true) {
                try {
                    fileOStream.getFD().sync();
                    break;
                } catch (SyncFailedException e) {
                    if (attempts == 3) {
                        System.out.println("Failed to fsync store file.");
                        throw e;
                    }
                }
            }
            fileOStream.close();
            Files.move(tmpFile.toPath(), storePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            System.out.println("Failed to store players.");
            e.printStackTrace(new PrintStream(System.out));
        }
    }
}
