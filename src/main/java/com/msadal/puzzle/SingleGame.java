package com.msadal.puzzle;

import com.msadal.puzzle.api.player.PlayerGameAPI;
import com.msadal.puzzle.domain.Player;
import com.msadal.puzzle.domain.PlayerType;
import com.msadal.puzzle.domain.Shape;

import java.util.concurrent.*;

public class SingleGame {

    private PlayerSeat playerOneSeat;
    private PlayerSeat playerTwoSeat;

    public void setPlayerOne(Player player, PlayerType type, PlayerGameAPI api) {
        this.playerOneSeat = new PlayerSeat(player, type, api);
    }

    public void setPlayerTwo(Player player, PlayerType type,  PlayerGameAPI api) {
        this.playerTwoSeat = new PlayerSeat(player, type, api);
    }

    private void callInParallel(Runnable playerOneAction, Runnable playerTwoAction) {
        Thread pOneThread = new Thread(playerOneAction);
        Thread pTwoThread = new Thread(playerTwoAction);
        pOneThread.start();
        pTwoThread.start();
        try {
            pOneThread.join();
            pTwoThread.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void sendMessage(final String message, final PlayerSeat recipient) {
        if (recipient != null) {
            recipient.getApi().sendMessage(message);
        } else {
            // Send to both players
            callInParallel(() -> {
                playerOneSeat.getApi().sendMessage(message);
            }, () -> {
                playerTwoSeat.getApi().sendMessage(message);
            });
        }
    }

    private void sendMessageToBoth(String message) {
        sendMessage(message, null);
    }

    private void callCountDown(long millis) {
        callInParallel(() -> {
            playerOneSeat.getApi().countingDown(millis);
        }, () -> {
            playerTwoSeat.getApi().countingDown(millis);
        });
    }

    private void readShape(long timeout) {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        Future<Shape> fShapePlayerOne = executor.submit(() -> playerOneSeat.getApi().readShape());
        Future<Shape> fShapePlayerTwo = executor.submit(() -> playerTwoSeat.getApi().readShape());
        executor.shutdown();
        try {
            Thread.sleep(timeout);
            executor.shutdownNow();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        fetchShape(playerOneSeat, fShapePlayerOne);
        fetchShape(playerTwoSeat, fShapePlayerTwo);
    }

    private Shape fetchShape(PlayerSeat playerSeat, Future<Shape> futureShape) {
        Shape shape = Shape.None;
        if (!futureShape.isDone()) {
            sendMessage("You've been waiting to long!", playerSeat);
        } else {
            try {
                shape = futureShape.get();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (ExecutionException e) {
                // consider as none decision
            }
        }
        playerSeat.setShape(shape);
        return shape;
    }

    private void handleGameEnd(PlayerSeat winner, PlayerSeat loser) {
        if (winner != null) {
            winner.getPlayer().getScore().incrementWins();
            sendMessage("You have won! Congratulations.", winner);
            loser.getPlayer().getScore().incrementsLosses();
            sendMessage("You have lost! Sorry!", loser);
        } else {
            playerOneSeat.getPlayer().getScore().incrementDraws();
            playerTwoSeat.getPlayer().getScore().incrementDraws();
            sendMessageToBoth("Nobody has won this time!");
        }
        sendFinishedGameSummary(winner);
    }

    private void sendFinishedGameSummary(PlayerSeat winner) {
        StringBuilder sb = new StringBuilder();
        sb.append("Finished game summary:\r\n");
        sb.append("Player one: ").append(playerOneSeat.getPlayer()).append(" playing as ").append(playerOneSeat.getType())
                .append(" showed ").append(playerOneSeat.getShape()).append("\r\n");
        sb.append("Player two: ").append(playerTwoSeat.getPlayer()).append(" playing as ").append(playerTwoSeat.getType())
                .append(" showed ").append(playerTwoSeat.getShape()).append("\r\n");
        if (winner != null) {
            sb.append("Winner is ").append(winner.getPlayer());
        } else {
            sb.append("No winner this time.");
        }
        sendMessageToBoth(sb.toString());
    }

    public void start() {
        sendMessageToBoth("Hello, game is about to start.");
        sendMessageToBoth(playerOneSeat.getPlayer().toString() + " vs " + playerTwoSeat.getPlayer().toString());
        callCountDown(TimeUnit.SECONDS.toMillis(5));

        readShape(TimeUnit.SECONDS.toMillis(1));

        PlayerSeat winner = null;
        PlayerSeat loser = null;
        if (playerOneSeat.getShape().beats(playerTwoSeat.getShape())) {
            winner = playerOneSeat;
            loser = playerTwoSeat;
        } else if (!playerOneSeat.getShape().equals(playerTwoSeat.getShape())) {
            winner = playerTwoSeat;
            loser = playerOneSeat;
        }

        handleGameEnd(winner, loser);
    }
}
