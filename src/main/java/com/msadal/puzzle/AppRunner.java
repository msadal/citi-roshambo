package com.msadal.puzzle;

import com.msadal.puzzle.api.GameAPI;
import com.msadal.puzzle.api.GameServerAPI;
import com.msadal.puzzle.api.GameShellAPI;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AppRunner {

    private final static int DEFAULT_PORT = 8888;
    private final static Path PLAYERS_STORE_PATH = Paths.get("../game-db/players.store");

    private GameShellAPI shell;
    private PlayerManager playerManager;
    private ServerSocket server;

    public AppRunner(int port) {
        this.shell = new GameShellAPI();
        this.playerManager = new PlayerManagerImpl(PLAYERS_STORE_PATH);
        try {
            System.out.println("Starting ServerSocket on port " + port);
            this.server = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Filed to start ServerSocket on port " + port);
            e.printStackTrace(System.out);
        }
    }

    private void handleRemoteGames() {
        try {
            while (server != null) {
                Socket socket = server.accept();
                GameAPI api = new GameServerAPI(socket);
                Thread remoteGameThread = new Thread(new GameSession(api, playerManager));
                remoteGameThread.start();
            }
        } catch (IOException e) {
            System.out.println("Closing ServerSocket");
            if (!server.isClosed()) {
                try {
                    server.close();
                } catch (IOException e1) {
                    System.out.println("Exception closing ServerSocket");
                }
            }
        }
    }

    private void closeServerSocket() {
        if (!server.isClosed()) {
            try {
                server.close();
            } catch (IOException e) {
                System.out.println("Exception closing ServerSocket");
            }
        }
    }

    public void start() {
        try {
            Thread shellThread = new Thread(new GameSession(shell, playerManager));
            Thread serverThread = new Thread(() -> {handleRemoteGames();});
            shellThread.start();
            serverThread.start();
            shellThread.join();

            System.out.println("Server is still running. <Control>+<C> to stop.");
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                closeServerSocket();
            }));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        playerManager.shutdown();
    }

    public static void main(String[] args) {
        int port = DEFAULT_PORT;
        if (args.length > 0) {
            try {
                port = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.out.println("Ignore incorrect port number parameter.");
            }
        }

        new AppRunner(port).start();
    }
}
