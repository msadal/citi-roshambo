package com.msadal.puzzle.domain;

import java.util.Random;

public enum Shape {

    None(0, 0),
    Rock(1, 3),
    Paper(2, 1),
    Scissors(3, 2);

    private int value;
    private int beaten;

    Shape(int value, int beaten) {
        this. value = value;
        this.beaten = beaten;
    }

    public int getValue() {
        return value;
    }

    public boolean beats(Shape shape) {
        return this.value != 0 && (this.beaten == shape.value || shape.value == 0);
    }

    public static Shape randomShape() {
        int result = (new Random().nextInt(1000) % 3) + 1;
        return getShape(result);
    }

    public static Shape getShape(int value) {
        switch(value) {
            case 1: return Rock;
            case 2: return Paper;
            case 3: return Scissors;
            default: return None;
        }
    }
}
