package com.msadal.puzzle.domain;

public enum PlayerType {

    Computer,
    Human
}
