package com.msadal.puzzle.domain;

import java.io.Serializable;

public class Score implements Serializable {

    public static final long serialVersionUID = 1L;

    private long wins;
    private long losses;
    private long draws;

    public long getWins() {
        return wins;
    }

    public long getLosses() {
        return losses;
    }

    public long getDraws() {
        return draws;
    }

    public synchronized void incrementWins() {
        ++wins;
    }

    public synchronized void incrementsLosses() {
        ++losses;
    }

    public synchronized void incrementDraws() {
        ++draws;
    }
}
