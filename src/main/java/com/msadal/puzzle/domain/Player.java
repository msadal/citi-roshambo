package com.msadal.puzzle.domain;

import java.io.Serializable;

public class Player implements Serializable {

    public static final long serialVersionUID = 1L;

    private String login;
    private String playerName;
    private Score score;

    public Player(String login, String playerName) {
        this.login = login;
        this.playerName = playerName;
        this.score = new Score();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Score getScore() {
        return score;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(login);
        if (playerName != null) {
            sb.append(" (").append(playerName).append(")");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!Player.class.isInstance(obj))
            return false;
        Player player = (Player) obj;
        return getLogin().equals(player.getLogin());
    }

    @Override
    public int hashCode() {
        return getLogin().hashCode();
    }
}
