package com.msadal.puzzle;

import com.msadal.puzzle.domain.Player;

import java.util.List;

public interface PlayerManager {
    Player get(String login);
    void put(Player player);
    List<Player> getAllPlayers();
    Player chooseOpponent(String yourLogin);
    void shutdown();
}
