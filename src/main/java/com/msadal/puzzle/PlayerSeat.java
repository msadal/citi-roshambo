package com.msadal.puzzle;

import com.msadal.puzzle.api.player.PlayerGameAPI;
import com.msadal.puzzle.domain.Player;
import com.msadal.puzzle.domain.PlayerType;
import com.msadal.puzzle.domain.Shape;

public class PlayerSeat {

    private Player player;
    private PlayerGameAPI api;
    private PlayerType type;
    private Shape shape;

    public PlayerSeat(Player player, PlayerType type, PlayerGameAPI api) {
        this.player = player;
        this.type = type;
        this.api = api;
    }

    public Player getPlayer() {
        return player;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public PlayerGameAPI getApi() {
        return api;
    }

    public PlayerType getType() {
        return type;
    }
}
