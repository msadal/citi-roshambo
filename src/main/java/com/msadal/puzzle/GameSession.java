package com.msadal.puzzle;

import com.msadal.puzzle.api.GameAPI;
import com.msadal.puzzle.domain.Player;
import com.msadal.puzzle.domain.PlayerType;
import com.msadal.puzzle.domain.Score;

import java.util.EnumSet;
import java.util.List;

import static com.msadal.puzzle.GameSession.Command.*;
import static com.msadal.puzzle.GameSession.Command.COMPUTER;
import static com.msadal.puzzle.GameSession.Command.QUIT;

public class GameSession implements Runnable {

    public enum Command {
        LOGIN,
        EXIT,
        PLAYERS,

        HUMAN,
        COMPUTER,
        QUIT;
    }


    private GameAPI api;
    private PlayerManager playerManger;

    public GameSession(GameAPI api, PlayerManager playerManger) {
        this.api = api;
        this.playerManger = playerManger;
    }

    @Override
    public void run() {
        api.sendMessage("Welcome to ReShemRo Game (Rock-Paper-Scissors)!");
        while (true) {
            EnumSet<Command> options = EnumSet.of(LOGIN, EXIT, PLAYERS);
            Command cmd = receiveCommand("Choose command [LOGIN | EXIT | PLAYERS]: ", options);
            if (EXIT.equals(cmd)) {
                exitSession();
                return;
            } else if (PLAYERS.equals(cmd)) {
                showPlayerScores();
            } else if (LOGIN.equals(cmd)) {
                loginAndPlay();
            }
        }
    }

    private void exitSession() {
        api.sendMessage("See you later!");
        api.close();
    }

    private void showPlayerScores() {
        StringBuilder info = new StringBuilder();
        info.append("    Players' Hall of Fame\n")
                .append("   Login (Player name): Wins | Draws | Losses\n")
                .append("==================================================\n");
        List<Player> players = playerManger.getAllPlayers();
        if (!players.isEmpty()) {
            players.stream()
                    .sorted((p1, p2) -> Long.compare(p2.getScore().getWins(), p1.getScore().getWins()))
                    .forEachOrdered(p -> {
                        Score score = p.getScore();
                        info.append(p.toString()).append(": ").append(score.getWins()).append(" | ")
                                .append(score.getDraws()).append(" | ").append(score.getLosses()).append("\n");
                    });

        } else {
            info.append("List is empty yet.");
        }
        api.sendMessage(info.toString());
    }

    private void loginAndPlay() {
        String login = receiveNotEmpty("Put your login: ");
        Player player = playerManger.get(login);
        if (player == null) {
            api.sendMessage("You are first time here. Adding you to player's list.");
            String playerName = api.receiveData("Put your name(optional): ");
            player = new Player(login, playerName);
            playerManger.put(player);
        }
        EnumSet<Command> options = EnumSet.of(HUMAN, COMPUTER, QUIT);
        while (true) {
            Command cmd = receiveCommand("Play as Human or Computer or Quit game [HUMAN | COMPUTER | QUIT]: ", options);
            if (QUIT.equals(cmd)) {
                api.sendMessage("You've just quit!");
                return;
            } else {
                PlayerType playAs = HUMAN.equals(cmd) ? PlayerType.Human : PlayerType.Computer;
                Player opponent = playerManger.chooseOpponent(player.getLogin());
                GameFactory.create(player, playAs, api, opponent).start();
            }
        }
    }


    private Command receiveCommand(String message, EnumSet<Command> options) {
        while (true) {
            try {
                String cmdName = api.receiveData(message);
                if (cmdName != null) {
                    Command cmd = valueOf(cmdName.toUpperCase());
                    if (options.contains(cmd)) {
                        return cmd;
                    } else {
                        api.sendMessage("Wrong option");
                    }
                }
            } catch (IllegalArgumentException e) {
                api.sendMessage("Unrecognised option");
            }
        }
    }

    private String receiveNotEmpty(String message) {
        while (true) {
            String data = api.receiveData(message);
            if (data != null && !data.isEmpty()) {
                return data;
            }
            api.sendMessage("Not empty data required.");
        }
    }
}
