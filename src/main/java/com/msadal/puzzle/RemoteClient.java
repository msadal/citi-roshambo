package com.msadal.puzzle;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

import static com.msadal.puzzle.api.GameServerAPI.RemoteCommand.*;

public class RemoteClient {

    private final static int DEFAULT_PORT = 8888;
    private final static String DEFAULT_SERVER_HOST = "localhost";

    private Socket socket;
    private Console console;

    public RemoteClient(String host, int port) {
        console = System.console();
        if (console == null) {
            throw new RuntimeException("Error: Console unavailable");
        }
        try {
            this.socket = new Socket(host, port);
        } catch (Exception e) {
            throw new RuntimeException("Failed to init socket", e);
        }
    }

    public void start() {
        DataInputStream iStream = null;
        PrintStream printStream = null;
        try {
            iStream = new DataInputStream(socket.getInputStream());
            printStream = new PrintStream(socket.getOutputStream());
            while (!socket.isClosed()) {
                int cmd = iStream.readInt();
                if (SEND.toInt() == cmd) {
                    handleSend(iStream);
                } else if (RECEIVE.toInt() == cmd) {
                    handleReceive(iStream, printStream);
                } else if (RECEIVE_INTERRUPTIBLE.toInt() == cmd) {
                    handleReceiveInterruptible(iStream, printStream);
                }
            }
        } catch (EOFException e) {
            console.printf("Remote client exits!");
        } catch (IOException e) {
            throw new RuntimeException("Error initializing streams", e);
        } finally {
            if (iStream != null) {
                try {
                    iStream.close();
                } catch (IOException e) {
                    // ignore this time
                }
            }
            if (printStream != null) {
                printStream.close();
            }
        }
    }

    private void handleReceiveInterruptible(DataInputStream iStream, PrintStream printStream) {
        try {
            boolean interrupted = false;
            String message  = iStream.readUTF();
            console.printf(message);

            long timeout = TimeUnit.SECONDS.toMillis(1) - 20;
            long startTime = System.currentTimeMillis();
            while (!console.reader().ready()) {
                if (System.currentTimeMillis() - startTime >= timeout) {
                    interrupted = true;
                    break;
                }
                Thread.sleep(10);
            }

            if (!interrupted) {
                printStream.println(console.readLine());
                printStream.flush();
            }
            console.printf("%n");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            console.printf("Error handling receive_interruptible command");
        }
    }

    private void handleReceive(DataInputStream iStream, PrintStream printStream) {
        try {
            String message = iStream.readUTF();
            String data = console.readLine(message);
            printStream.println(data);
            printStream.flush();
        } catch (IOException e) {
            console.printf("Error handling receive command");
        }
    }

    private void handleSend(DataInputStream iStream) {
        try {
            String message = iStream.readUTF();
            console.printf(message);
        } catch (IOException e) {
            console.printf("Error handling send command");
        }
    }

    private void handleClose(DataInputStream iStream, PrintStream printStream) {
        if (iStream != null) {
            try {
                iStream.close();
            } catch (IOException e) {
                // error closing stream
            }
        }
        if (printStream != null) {
            printStream.close();
        }
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                System.out.println("Error closing socket");
                System.exit(1);
            }
        }
    }

    public static void main(String[] args) {
        try {
            String host = DEFAULT_SERVER_HOST;
            int port = DEFAULT_PORT;
            if (args.length > 0) {
                host = args[0];
            }
            if (args.length > 1) {
                try {
                    port = Integer.parseInt(args[1]);
                } catch (NumberFormatException e) {
                    System.out.println("Ignore incorrect port number parameter.");
                }
            }

            new RemoteClient(host, port).start();
        } catch (Exception e) {
            System.out.println("Failed to init remote client ");
            e.printStackTrace(System.out);
        }
    }
}
