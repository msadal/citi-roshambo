package com.msadal.puzzle;

import com.msadal.puzzle.api.*;
import com.msadal.puzzle.api.player.ComputerAsHumanGameAPI;
import com.msadal.puzzle.api.player.DumbComputerGameAPI;
import com.msadal.puzzle.api.player.HumanGameAPI;
import com.msadal.puzzle.api.player.PlayerGameAPI;
import com.msadal.puzzle.domain.Player;
import com.msadal.puzzle.domain.PlayerType;

public class GameFactory {

    public static SingleGame create(Player pOne, PlayerType pOneAs, GameAPI api, Player pTwoComputer) {
        SingleGame game = new SingleGame();
        PlayerGameAPI pOneAPI =
                PlayerType.Human.equals(pOneAs)? new HumanGameAPI(api): new ComputerAsHumanGameAPI(api);
        PlayerGameAPI pTwoAPI = new DumbComputerGameAPI();

        game.setPlayerOne(pOne, pOneAs, pOneAPI);
        game.setPlayerTwo(pTwoComputer, PlayerType.Computer, pTwoAPI);
        return game;
    }
}
