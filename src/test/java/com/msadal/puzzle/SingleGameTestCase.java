package com.msadal.puzzle;

import com.msadal.puzzle.api.GameAPI;
import com.msadal.puzzle.api.player.HumanGameAPI;
import com.msadal.puzzle.api.player.PlayerGameAPI;
import com.msadal.puzzle.domain.Player;
import com.msadal.puzzle.domain.PlayerType;
import com.msadal.puzzle.domain.Shape;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static junit.framework.TestCase.assertEquals;

public class SingleGameTestCase {

    private static class TestPlayerAPI implements PlayerGameAPI {
        private PlayerGameAPI api;
        private Shape shape;
        private PrintStream pStream;
        private ByteArrayOutputStream bos;

        public TestPlayerAPI(Shape shape) {
            this.shape = shape;
            this.api = new HumanGameAPI(new GameAPI() {
                @Override
                public void sendMessage(String message) {
                    pStream.printf(message + "%n");
                }

                @Override
                public String receiveData(String message) {
                    pStream.printf(message);
                    return null;
                }

                @Override
                public String receiveDataInterruptible(String message) throws InterruptedException {
                    pStream.printf(message);
                    return null;
                }

                @Override
                public void close() {
                }
            });
            this.bos = new ByteArrayOutputStream();
            this.pStream = new PrintStream(bos);
        }

        @Override
        public void sendMessage(String message) {
            api.sendMessage(message);
        }

        @Override
        public void countingDown(long millis) {
            api.countingDown(millis);
        }

        @Override
        public Shape readShape() {
            return shape;
        }

        public String getOutputAsString() {
            return bos.toString();
        }
    }

    @Test
    public void testPlayerOneWins() {
        SingleGame game = new SingleGame();
        Player p1 = new Player("p1", "Player1");
        Player p2 = new Player("p2", "Computer");
        TestPlayerAPI p1Api = new TestPlayerAPI(Shape.Scissors);
        game.setPlayerOne(p1, PlayerType.Human, p1Api);
        game.setPlayerTwo(p2, PlayerType.Computer, new TestPlayerAPI(Shape.Paper));
        game.start();

        assertEquals(1, p1.getScore().getWins());
        assertEquals(0, p1.getScore().getDraws());
        assertEquals(1, p2.getScore().getLosses());
        assertEquals(0, p2.getScore().getDraws());
        String expectedOutput = "Hello, game is about to start.\r\n" +
                "p1 (Player1) vs p2 (Computer)\r\n" +
                "Ready... Steady...\r\n" +
                "Ro!\r\n" +
                "Sham!\r\n" +
                "Bo!\r\n" +
                "You have won! Congratulations.\r\n" +
                "Finished game summary:\r\n" +
                "Player one: p1 (Player1) playing as Human showed Scissors\r\n" +
                "Player two: p2 (Computer) playing as Computer showed Paper\r\n" +
                "Winner is p1 (Player1)\r\n";
        String actualOutput = p1Api.getOutputAsString();
        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testPlayerTwoWins() {
        SingleGame game = new SingleGame();
        Player p1 = new Player("p1", "Mariusz Sadal");
        Player p2 = new Player("p2", "Deep Blue");
        TestPlayerAPI p1Api = new TestPlayerAPI(Shape.Scissors);
        game.setPlayerOne(p1, PlayerType.Human, p1Api);
        game.setPlayerTwo(p2, PlayerType.Computer, new TestPlayerAPI(Shape.Rock));
        game.start();

        assertEquals(1, p1.getScore().getLosses());
        assertEquals(0, p1.getScore().getDraws());
        assertEquals(1, p2.getScore().getWins());
        assertEquals(0, p2.getScore().getDraws());
        String expectedOutput = "Hello, game is about to start.\r\n" +
                "p1 (Mariusz Sadal) vs p2 (Deep Blue)\r\n" +
                "Ready... Steady...\r\n" +
                "Ro!\r\n" +
                "Sham!\r\n" +
                "Bo!\r\n" +
                "You have lost! Sorry!\r\n" +
                "Finished game summary:\r\n" +
                "Player one: p1 (Mariusz Sadal) playing as Human showed Scissors\r\n" +
                "Player two: p2 (Deep Blue) playing as Computer showed Rock\r\n" +
                "Winner is p2 (Deep Blue)\r\n";
        String actualOutput = p1Api.getOutputAsString();
        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testPlayerOneLossesNoInput() {
        SingleGame game = new SingleGame();
        Player p1 = new Player("p1", null);
        Player p2 = new Player("p2", "Deep Blue");
        TestPlayerAPI p1Api = new TestPlayerAPI(Shape.None);
        game.setPlayerOne(p1, PlayerType.Human, p1Api);
        game.setPlayerTwo(p2, PlayerType.Computer, new TestPlayerAPI(Shape.Scissors));
        game.start();

        assertEquals(1, p1.getScore().getLosses());
        assertEquals(0, p1.getScore().getDraws());
        assertEquals(1, p2.getScore().getWins());
        assertEquals(0, p2.getScore().getDraws());
        String expectedOutput = "Hello, game is about to start.\r\n" +
                "p1 vs p2 (Deep Blue)\r\n" +
                "Ready... Steady...\r\n" +
                "Ro!\r\n" +
                "Sham!\r\n" +
                "Bo!\r\n" +
                "You have lost! Sorry!\r\n" +
                "Finished game summary:\r\n" +
                "Player one: p1 playing as Human showed None\r\n" +
                "Player two: p2 (Deep Blue) playing as Computer showed Scissors\r\n" +
                "Winner is p2 (Deep Blue)\r\n";
        String actualOutput = p1Api.getOutputAsString();
        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testDraw() {
        SingleGame game = new SingleGame();
        Player p1 = new Player("p1", null);
        Player p2 = new Player("p2", null);
        TestPlayerAPI p1Api = new TestPlayerAPI(Shape.Rock);
        game.setPlayerOne(p1, PlayerType.Human, p1Api);
        game.setPlayerTwo(p2, PlayerType.Computer, new TestPlayerAPI(Shape.Rock));
        game.start();

        assertEquals(0, p1.getScore().getWins());
        assertEquals(1, p1.getScore().getDraws());
        assertEquals(0, p2.getScore().getWins());
        assertEquals(1, p2.getScore().getDraws());
        String expectedOutput = "Hello, game is about to start.\r\n" +
                "p1 vs p2\r\n" +
                "Ready... Steady...\r\n" +
                "Ro!\r\n" +
                "Sham!\r\n" +
                "Bo!\r\n" +
                "Nobody has won this time!\r\n" +
                "Finished game summary:\r\n" +
                "Player one: p1 playing as Human showed Rock\r\n" +
                "Player two: p2 playing as Computer showed Rock\r\n" +
                "No winner this time.\r\n";
        String actualOutput = p1Api.getOutputAsString();
        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testPlayerOnaAsComputerWins() {
        SingleGame game = new SingleGame();
        Player p1 = new Player("p1", null);
        Player p2 = new Player("p2", null);
        TestPlayerAPI p1Api = new TestPlayerAPI(Shape.Paper);
        game.setPlayerOne(p1, PlayerType.Computer, p1Api);
        game.setPlayerTwo(p2, PlayerType.Computer, new TestPlayerAPI(Shape.Rock));
        game.start();

        assertEquals(1, p1.getScore().getWins());
        assertEquals(0, p1.getScore().getDraws());
        assertEquals(1, p2.getScore().getLosses());
        assertEquals(0, p2.getScore().getDraws());
        String expectedOutput = "Hello, game is about to start.\r\n" +
                "p1 vs p2\r\n" +
                "Ready... Steady...\r\n" +
                "Ro!\r\n" +
                "Sham!\r\n" +
                "Bo!\r\n" +
                "You have won! Congratulations.\r\n" +
                "Finished game summary:\r\n" +
                "Player one: p1 playing as Computer showed Paper\r\n" +
                "Player two: p2 playing as Computer showed Rock\r\n" +
                "Winner is p1\r\n";
        String actualOutput = p1Api.getOutputAsString();
        assertEquals(expectedOutput, actualOutput);
    }
}
