package com.msadal.puzzle.dao;

import com.msadal.puzzle.domain.Player;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SerialPlayerDaoTestCase {

    private PlayerDao dao;

    @Before
    public void init() {
        Path storePath = Paths.get("../dao-store/test.store");
        if (Files.exists(storePath)) {
            try {
                Files.delete(storePath);
            } catch (IOException e) {
                // just ignore this time
            }
        }
        dao = new SerialPlayerDaoImpl(storePath);
    }

    @Test
    public void testStoreLoadSuccess() {
        Player human = new Player("msadal", "Mariusz Sadal");
        Player computer = new Player("pc", "Deep Blue");
        human.getScore().incrementWins();
        human.getScore().getDraws();
        computer.getScore().incrementsLosses();
        computer.getScore().incrementDraws();
        List<Player> list2Store = new LinkedList<>();
        list2Store.add(human);
        list2Store.add(computer);
        dao.store(list2Store);

        List<Player> loadedList = dao.load();
        assertEquals(2, loadedList.size());
        assertEquals("msadal", loadedList.get(0).getLogin());
        assertEquals("Mariusz Sadal", loadedList.get(0).getPlayerName());
        assertEquals(1, loadedList.get(0).getScore().getWins());
        assertEquals("pc", loadedList.get(1).getLogin());
        assertEquals("Deep Blue", loadedList.get(1).getPlayerName());
        assertEquals(1, loadedList.get(1).getScore().getLosses());
    }

    @Test
    public void testLoadNoFile() {
        List<Player> loadedList = dao.load();
        assertEquals(0, loadedList.size());
    }

}
