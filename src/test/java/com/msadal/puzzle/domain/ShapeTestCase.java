package com.msadal.puzzle.domain;

import junit.framework.TestCase;
import org.junit.Test;

import static com.msadal.puzzle.domain.Shape.*;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ShapeTestCase {

    @Test
    public void testRock() {
        assertTrue(Rock.beats(Scissors));
        assertTrue(Rock.beats(None));
        assertFalse(Rock.beats(Paper));
        assertFalse(Rock.beats(Rock));
    }

    @Test
    public void testPaper() {
        assertTrue(Paper.beats(Rock));
        assertTrue(Paper.beats(None));
        assertFalse(Paper.beats(Scissors));
        assertFalse(Paper.beats(Paper));
    }

    @Test
    public void testScissors() {
        assertTrue(Scissors.beats(Paper));
        assertTrue(Scissors.beats(None));
        assertFalse(Scissors.beats(Rock));
        assertFalse(Scissors.beats(Scissors));
    }

    @Test
    public void testNone() {
        assertFalse(None.beats(Paper));
        assertFalse(None.beats(None));
        assertFalse(None.beats(Rock));
        assertFalse(None.beats(Scissors));
    }
}
