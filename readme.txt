This is the Ro-Sham-Bo game (the popular Rock-Paper-Scissors game).

Game was implemented as a text console application and uses System.console implementation from Java 1.6. That's why application
doesn't work from IDEs (only from standard text console).
Application can be started in two modes:
 - as server with only one game session active (call run-app-with-server.bat to start),
 - as remote client - connects to active server (call run-remote-client.bat to start).

Implementing this application I made most effort to develop extensible solution in terms of APIs. Now we have simple
shell and socket clients but it's very easy to add new once. It wouldn't be complicated if we wanted to add REST client.
We would only have to implement GameAPI interface on a server side and pass this API instance to new GameSession thread.
I gave up this idea because I was not allowed to use any third party libraries and I wasn't keen in implementing my own
REST service in pure java. I've chosen less complex ServerSocket example to demonstrate this feature.

I also added simple persistence for the players list and their scores. I used java serialization mechanism for that.
Source code is also available as a public BitBucket repository.
To clone it call:
git clone https://msadal@bitbucket.org/msadal/citi-roshambo.git

Have fun :).